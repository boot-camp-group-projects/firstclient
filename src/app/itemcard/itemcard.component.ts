import { Component, OnInit } from '@angular/core';
import {ItemServiceService} from '../item-service.service';

@Component({
  selector: 'app-itemcard',
  templateUrl: './itemcard.component.html',
  styleUrls: ['./itemcard.component.css']
})
export class ItemcardComponent implements OnInit {
items: Array<any>;
  constructor(private ItemService: ItemServiceService) { }

  ngOnInit() {
    this.ItemService.getAllItems().subscribe(data => {
      this.items = data;
    });
  }
}
