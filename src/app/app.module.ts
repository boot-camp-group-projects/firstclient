import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemcardComponent } from './itemcard/itemcard.component';
import {HttpClientModule} from '@angular/common/http';
import {ItemServiceService} from './item-service.service';

@NgModule({
  declarations: [
    AppComponent,
    ItemcardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ItemServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
